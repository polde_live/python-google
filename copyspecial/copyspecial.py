#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re
import os
import shutil
import subprocess

"""Copy Special exercise
"""

# Part A
def get_special_paths(dir):
    """Returns a list of the absolute paths of the special files in the given directory"""
    all_files = os.listdir(dir)
    special_files = []
    for fname in all_files:
        if re.search('__\w+__',fname) is not None:
            pth = os.path.join(dir, fname)
            apth = os.path.abspath(pth)
            special_files.append(apth)
    return(special_files)


# Part B
def copy_to(paths, dir):
    """Given a list of paths, copies those files into the given directory """
    if not os.path.exists(dir):
        os.mkdir(dir)
    for path in paths:
        shutil.copy(path, dir)
        
# Part C
def zip_to(paths, dir):
    """Given a list of paths, zip those files up into the given zipfile"""
    if not os.path.exists(dir):
        os.mkdir(dir)
    target = os.path.join(dir, 'files.zip') # where files are to be stored
    cmd = 'zip -j %s ' % target # windows zip commands
    cmd = cmd + ' '.join(paths)
    subprocess.call(cmd)

def main():
    # This basic command line argument parsing code is provided.
    # Add code to call your functions below.

    # Make a list of command line arguments, omitting the [0] element
    # which is the script itself.
    args = sys.argv[1:]
    if not args:
        print "usage: [--todir dir][--tozip zipfile] dir [dir ...]";
        sys.exit(1)

    # todir and tozip are either set from command line
    # or left as the empty string.
    # The args array is left just containing the dirs.
    todir = ''
    if args[0] == '--todir':
        todir = args[1]
        del args[0:2]

    tozip = ''
    if args[0] == '--tozip':
        tozip = args[1]
        del args[0:2]

    if len(args) == 0:
        print "error: must specify one or more dirs"
        sys.exit(1)

    dir = args[0]
    paths = get_special_paths(dir)
    if len(todir)>0:
        copy_to(paths, todir)
    elif len(tozip) > 0:
        zip_to(paths, tozip)
    else:
        print('\n'.join(paths))
  
if __name__ == "__main__":
  main()
